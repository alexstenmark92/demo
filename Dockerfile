# Dockerfile for One-Time Secret
# http://onetimesecret.com

FROM debian:stretch-slim

MAINTAINER Alexander Stenmark	 <alexander.stenmark@elits.com>

# Install dependencies
RUN DEBIAN_FRONTEND=noninteractive \
  apt-get update && \
  apt-get install -y \
    ruby \
    ruby-dev \
    ruby-bundler \
    redis-server \
    curl \
    build-essential \
  && rm -rf /var/lib/apt/lists/*

# OTS pre-installation
RUN set -ex && \
  # Add ots user
  useradd -U -s /bin/false ots && \
  \
  # Create directories
  mkdir -p /etc/onetime /var/log/onetime /var/run/onetime /var/lib/onetime /var/lib/onetime/redis && \
  chown ots /etc/onetime /var/log/onetime /var/run/onetime /var/lib/onetime /var/lib/onetime/redis

USER ots

# Download and install latest OTS
RUN set -ex && \
  curl -L https://github.com/onetimesecret/onetimesecret/archive/273216e1b09d8227b5250af8c623a4f2f8e53780.tar.gz -o /tmp/ots.tar.gz && \
  tar xzf /tmp/ots.tar.gz -C /var/lib/onetime --strip-components=1 && \
  rm /tmp/ots.tar.gz
  #&& \
  #cd /var/lib/onetime

COPY --chown=ots:ots helpers.rb /var/lib/onetime/lib/onetime/app/web/views/helpers.rb
COPY --chown=ots:ots Gemfile /var/lib/onetime/Gemfile
COPY --chown=ots:ots Gemfile.lock /var/lib/onetime/Gemfile.lock

RUN set -ex && \
  cd /var/lib/onetime && \
  bundle install --frozen --deployment --without=dev --gemfile /var/lib/onetime/Gemfile && \
  cp -R etc/* /etc/onetime/

# Copy our own config files over the config files that OTS put in there
# By default, these are owned by root, but we tell Docker to have the ots user own them instead
COPY --chown=ots:ots config.example /etc/onetime/config
COPY --chown=ots:ots redis.conf.example /etc/onetime/redis.conf

# Copy modified mustasche files to alter pages
COPY --chown=ots:ots templates/colonel/* /var/lib/onetime/templates/colonel/
COPY --chown=ots:ots templates/email/* /var/lib/onetime/templates/email/
COPY --chown=ots:ots templates/web/* /var/lib/onetime/templates/web/

# Copy modified css files to alter pages
COPY --chown=ots:ots public/web/css/main.css /var/lib/onetime/public/web/css/main.css
COPY --chown=ots:ots public/web/img/favicon.png /var/lib/onetime/public/web/img/favicon.png

COPY entrypoint.sh /usr/bin/

VOLUME /etc/onetime /var/lib/onetime/redis

EXPOSE 7143/tcp

ENTRYPOINT /usr/bin/entrypoint.sh
