#!/bin/bash

# NOTE: sed -i doesn't work when configs are mount points

if grep -q ":secret: CHANGEME" /etc/onetime/config; then
  echo "Setting OTS secret: $OTS_PASSWORD"
  cp /etc/onetime/config{,.bak}
  sed "s/:secret:.*\$/:secret: $OTS_PASSWORD/" /etc/onetime/config.bak > /etc/onetime/config
fi

# set redis password if needed
if grep -q "redis://user:CHANGEME@127.0.0.1:7179" /etc/onetime/config && \
    grep -q "requirepass CHANGEME" /etc/onetime/redis.conf; then
  echo "Setting Redis password: $REDIS_PASSWORD"
  cp /etc/onetime/config{,.bak}
  cp /etc/onetime/redis.conf{,.bak}
  sed "s/requirepass.*\$/requirepass $REDIS_PASSWORD/" /etc/onetime/redis.conf.bak > /etc/onetime/redis.conf
  sed "s/redis:\/\/user:CHANGEME/redis:\/\/user:$REDIS_PASSWORD/" /etc/onetime/config.bak > /etc/onetime/config
fi

if grep -q ":host: CHANGEME" /etc/onetime/config; then
  echo "Setting hostname: $OTS_HOST"
  cp /etc/onetime/config{,.bak}
sed "s/:host: CHANGEME/:host: $OTS_HOST/" /etc/onetime/config.bak > /etc/onetime/config
fi

#set timezone
#ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ /etc/timezone

redis-server /etc/onetime/redis.conf
cd /var/lib/onetime && bundle exec thin -e dev -R config.ru -p 7143 start
