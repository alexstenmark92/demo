README

To set a specific password rather than have one automatically set, go into config.example and change the secret CHANGEME to the password of your choice, and then proceeed to the redis.conf.example and change the requirepass CHANGEME to a password of your choice.